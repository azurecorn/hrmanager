<!doctype html>
<html lang="en">
    <head>
        @include('includes.head')
    </head>
    <body>
        @include('includes.header')

        @include('includes.flashmessage')

        @yield('content')

        @include('includes.errors')

        @include('includes.footer')

        @include('includes.footerscripts')
    </body>

</html>