@extends('layouts.master')

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Register</div>
					<div class="panel-body">
						<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
							{!! csrf_field() !!}

							<div class="form-group has-feedback">
								<label class="col-md-4 control-label">Name</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="name" value="{{ old('name') }}">
								</div>
							</div>

							<div class="form-group has-feedback">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-6">
									<input type="email" class="form-control" name="email" value="{{ old('email') }}">
									<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
								</div>
							</div>

							<div class="form-group has-feedback">
								<label class="col-md-4 control-label">Password</label>
								<div class="col-md-6">
									<input type="password" class="form-control" name="password">
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
								</div>
							</div>

							<div class="form-group has-feedback">
								<label class="col-md-4 control-label">Confirm Password</label>
								<div class="col-md-6">
									<input type="password" class="form-control" name="password_confirmation">
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
								</div>
							</div>

							<div class="form-group has-feedback">
								<div class="col-md-3 col-md-offset-7">
									<button type="submit" class="btn btn-primary btn-block btn-flat">
										Register
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
