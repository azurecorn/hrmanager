<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Sign Up Confirmation</title>
    </head>
    <body>
        <h1>New Job Submitted - Moderation Needed</h1>
        <p>
            Hello moderator. We just got a job submission from the new user.
            <br>
            <br>
            Non-verifyed user {{ $job->username }} submitted a job:
            <br>
            <br>Title: {{ $job->title }}
            <br>Description: {{ $job->description }}
            <br>
            <br>
            You can <a href='{{ url("moderate/{$job->hashed_id}/approve") }}'>approve this submission</a> or <a href='{{ url("moderate/{$job->hashed_id}/markspam") }}'>mark it as a spam</a>
        </p>
    </body>
</html>