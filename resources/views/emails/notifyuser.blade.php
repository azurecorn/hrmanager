<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>First Job Submission Notification</title>
    </head>
    <body>
        <h1>First Job Submission Notification</h1>
        <p>
            Dear {{ $user->name }}, thanks for posting your very first job offer. Your posting will be check by our moderators and as soon we approve it, it will be listed!
        </p>
    </body>
</html>