<!-- Navigation -->
<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Hr Manager Home</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">

            @if (Auth::check())
                <li>
                    <a href="{{ url('/auth/logout') }}">Logout {{Auth::user()->name}}</a>
                </li>
            @else
                <li>
                    <a href="{{ url('/auth/login') }}">Login</a>
                </li>
                <li>
                    <a href="{{ url('/auth/register') }}">Register</a>
                </li>
            @endif
                <li>
                    <a href="#">About Us</a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('{{URL::asset('img/home-bg.jpg')}}')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Job Listing</h1>
                    <hr class="small">
                    <span class="subheading">HR Manager Portal</span>
                </div>
            </div>
        </div>
    </div>
</header>