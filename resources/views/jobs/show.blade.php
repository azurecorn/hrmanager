@extends('layouts.master')

@section('content')

        <!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <div class="post-preview">
                <h2 class="post-title">{{ $job->title }}</h2>
                <p class="post-meta">Posted by {{ $job->user->name }} on {{ $job->created_at }}</p>
                <p class="post">{{ $job->description }}</p>
            </div>
        </div>
    </div>
</div>
@stop