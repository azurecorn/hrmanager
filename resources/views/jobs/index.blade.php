@extends('layouts.master')

@section('content')

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <p>Here's a list of all jobs. <a href="{{ route('job.create') }}">Add a new one?</a></p>
            <hr>
        @foreach($jobs as $job)
            <div class="post-preview">
                <a href="{{ route('job.show', $job->id) }}">
                    <h2 class="post-title">
                        {{ $job->title }}
                    </h2>
                </a>
                <p class="post-meta">Posted by {{ $job->user->name }} on {{ $job->created_at }}</p>
            </div>
            <hr>
        @endforeach
        </div>
    </div>
</div>
@stop