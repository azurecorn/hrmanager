<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Job protected routes
Route::group(['middleware' => 'auth'], function() {
    Route::get('job/create', ['as' => 'job.create', 'uses' => 'JobBoardController@create']);
    Route::post('job', ['as' => 'job.store', 'uses' => 'JobBoardController@store']);
});
// Public routes
Route::get('/', ['as' => 'home', 'uses' => 'JobBoardController@index']);
Route::get('jobs', ['as' => 'jobs', 'uses' => 'JobBoardController@index']);
Route::get('job/{job}', ['as' => 'job.show', 'uses' => 'JobBoardController@show']);

//Moderate submissions
Route::get('moderate/{hashed_id}/{action}', ['as' => 'moderation', 'uses' => 'JobBoardController@moderateJob']);
