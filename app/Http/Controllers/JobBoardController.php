<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Job;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\JobRequest;

class JobBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::with('user')->whereStatus(env('JOB_STATUS_PUBLISHED'))->orderBy('created_at', 'desc')->get();
        return view('jobs.index')->withJobs($jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jobs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Redirect
     */
    public function store(JobRequest $request)
    {
        $job = Job::create([
            'user_id' => Auth::user()->id,
            'title' => $request->input('title'),
            'description' => $request->input('description')
        ]);

        if($job->status){
            flash('You\'ve published a new job!');
        } else{
            flash('Your submission will be review by our moderator and published if approved.');
        }


        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        return view('jobs.show')->withJob($job);
    }

    /**
     * Moderate Job, approve or mark as spam
     *
     * @param  string $hash_id
     * @param  string  $action
     * @return \Redirect
     */
    public function moderateJob($hashed_id, $action)
    {
        $job = Job::whereHashedId($hashed_id)->firstOrFail();

        if($job->status == env('JOB_STATUS_TO_BE_MODERATED')){
            if($action == "approve"){
                $job->status = env('JOB_STATUS_PUBLISHED');
                $job->hashed_id = null;
                $job->save();
                flash('You\'ve approved this job!');
            } elseif ($action == "markspam"){
                $job->status = env('JOB_STATUS_SPAM');
                $job->hashed_id = null;
                $job->save();
                flash('You\'ve marked as spam this job!');
            }
        }

        return redirect()->route('home');
    }
}
