<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Job extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'hr_jobs';

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'hashed_id'
    ];

    protected $dates = ['created_at','updated_at'];

    /**
     * Job belongs to user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getCreatedAtAttribute($value)
    {
       return  Carbon::createFromFormat('Y-m-d H:i:s', $value)->toFormattedDateString();
    }
}
