<?php

namespace App\Observers;
use App\Job;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

/**
 * Observes the Job model
 */
class JobObserver
{

    /**
     * Function will be triggered when a job is creating
     * @param Job $model
     */
    public function creating($model)
    {
        $user = Auth::user();

        //get number of published jobs for this user
        $count_confirmed_jobs = Job::where('user_id', $user->id)
                                    ->where('status', env('JOB_STATUS_PUBLISHED'))
                                    ->count();

        //is there was previously approved jobs, approve this one too. Else, mark this one as "to be moderated" and sand mail to inform user
        if ($count_confirmed_jobs){
            $model->status = env('JOB_STATUS_PUBLISHED');
            $model->hashed_id = null;
        } else {
            $model->status = env('JOB_STATUS_TO_BE_MODERATED');
            $model->hashed_id = md5($model->title.Auth::user()->id.time());

            //notify user
            Mail::queue('emails.notifyuser', ['user' => $user], function($message) use ($user){
                $message->from(env('MAIL_USERNAME'), env('MAIL_SENDER_NAME'))
                        ->to($user->email, $user->name)
                        ->subject('HR Manager Portal Info Mail');
            });
        }
    }

    /**
     * Function will be triggered when a job is created
     * @param Job $model
     */
    public function created($model)
    {
        $user = Auth::user();

        //get number of published jobs for this user
        $count_confirmed_jobs = Job::where('user_id', $user->id)
            ->where('status', env('JOB_STATUS_PUBLISHED'))
            ->count();

        //is there wasn't previously approved jobs, sand mail to moderator
        if (!$count_confirmed_jobs) {

            //notify moderator

            $model->username = $user->name;

            Mail::queue('emails.notifymoderator', ['job' => $model], function($message){
                $message->from(env('MAIL_USERNAME'), env('MAIL_SENDER_NAME'))
                    ->to(env('MODERATOR_EMAIL'))
                    ->subject('HR Manager Portal - Job Moderation');
            });
        }
    }

}