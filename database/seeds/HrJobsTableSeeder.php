<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\User;

class HrJobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 10;
        $users = User::all();
        $user_ids = [];

        foreach ($users as $user) {
            $user_ids[] = $user->getAttribute('id');
        }

        $count_users = count($user_ids);

        for ($i = 0; $i < $limit; $i++) {
            DB::table('hr_jobs')->insert([
                'user_id' => $user_id = $user_ids[rand(0, $count_users-1)],
                'title' => $title = $faker->sentence($nbWords = 4, $variableNbWords = true),
                'description' => $faker->text($maxNbChars = 400),
                'status' => 1,
                'hashed_id' => md5($title.$user_id.time()),
                'created_at' => Carbon::now(),
            ]);
        }
    }
}